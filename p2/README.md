# lis4381

## Zachary Brown

### Project #2 Requirements:
1. index.php
2. edit_petstore.php
3. edit_petstore_process.php
4. rssfeed.php

#### Project Screenshots:

*Screenshot of index.php*:

![index.php](img/index.png)

*Screenshot of edit_process.php*:

![edit_petstore_process.php](img/edit_process.png)

*Screenshot of edited page*:

![edited page](img/edit.png)

*Scrrenshot of rssfeed*:

![rssfeed](img/rssfeed.png)






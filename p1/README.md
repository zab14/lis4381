> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4381

## Zachary Brown

### Project #1 Requirements:
1. First user interface running.
2. Second user interface running.


#### Assignment Screenshots:

*Screenshot of first user interface*:

![First user interface screenshot](img/app1.png)

*Screenshot of second user interface*:

![Second user interface screenshot](img/app2.png)





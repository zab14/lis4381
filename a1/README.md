> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# lis4381

## Zachary Brown

### Assignment #1 Requirements:

*Sub-Heading:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Chapter Questions (Chs 1,2)

#### README.md file should include the following items:

* Screenshot of Ampps Installation.
* Screenshot of java Hello.
* Screenshot of running Andorid Studio- My First App.
* git commands w/short descriptions.
* Bitbucket repo links a):this assignment and b): the completed tutprials above.

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init- This command creates an empty Git repository.
2. git status- List the files you've changed and those you still need to add or commit.
3. git add- This command updates the index using the current content found in the working tree, to prepare the content staged for the next commit.
4. git commit- Commit changes to head (but not yet to the remote repository).
5. git push- Send changes to the master branch of your remote repository.
6. git pull- Incorporates changes from a remote repository into the current branch. 
7. git tags- You can use tagging to mark a significant changeset, such as a release.

#### Assignment Screenshots:

[Screenshot of Running AMPPS](http://localhost "PHP Localhost")*:

![AMPPS Installation Screenshot](img/ampps.png) 


*Screenshot of running java Hello*:

![JDK Installation Screenshot](img/jdk_install.png)

*Screenshot of Android Studio - My First App*:

![Android Studio Installation Screenshot](img/android.png)


#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/username/bitbucketstationlocations/ "Bitbucket Station Locations")

*Tutorial: Request to update a teammate's repository:*
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/username/myteamquotes/ "My Team Quotes Tutorial")

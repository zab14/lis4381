Project documentation goes here, using Markdown syntax.
# lis4381

## Zachary Brown

### Assignment #4 Requirements:
1. Home page.
2. Pet store.



#### Assignment Screenshots:

*Screenshot of home page and carousel*:

![Home page screenshot](img/homepg.png)

*Screnshot of pet store page*:

![Pet store page](img/petstore.png)

*My Portfolio link:*
[Portfolio webpage link](http://localhost/repos/lis4381/index.php)